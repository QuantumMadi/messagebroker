using RabbitMQ.Client;

namespace Otus.Teaching.Pcf.Administration.WebHost.RabbitMqConfiguration
{
    public class RabbitMqConnection : System.IDisposable
    {
        private readonly RabbitMqConfigurationOptions _configuration;
        private  IConnection connection;
        public RabbitMqConnection(RabbitMqConfigurationOptions rabbitMqOptions)
        {
            _configuration = rabbitMqOptions;
        }

        public void Dispose()
        {
            if (connection.IsOpen)
            {
                connection.Close();
                connection.Dispose();
            }  
        }

        public IConnection GetConnection()
        {
            var factory = new ConnectionFactory
            {
                HostName = _configuration.HostName,
                UserName = _configuration.UserName,
                Password = _configuration.Password,
                VirtualHost = _configuration.VirtualHost
            };
            connection = factory.CreateConnection();
            return connection;
        }
    }
}