using System;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace  Otus.Teaching.Pcf.Administration.Core.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IRepository<Employee> _employeeRepository;
        public EmployeeService(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        public async Task UpdatePromocodes(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee != null)
            {
                employee.AppliedPromocodesCount++;
                await _employeeRepository.UpdateAsync(employee);
            }
        }
        public static Guid ParseGuid(string id)
        {
            Guid.TryParse(id, out Guid result);
            return result;
        }
    }
}