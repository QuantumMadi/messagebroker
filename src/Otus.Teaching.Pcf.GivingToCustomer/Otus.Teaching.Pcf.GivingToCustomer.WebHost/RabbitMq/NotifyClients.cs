using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.RabbitMq
{
    public class NotifyClients : BackgroundService
    {

        private IConnection _connection;
        private IModel _channel;
        private string _queueName;
        private IServiceProvider _provider;
        public NotifyClients(IOptions<RabbitMqConfigurationOptions> configuration,
        IServiceProvider provider)
        {
            _provider = provider;
            var factory = new ConnectionFactory
            {
                HostName = configuration.Value.HostName,
                UserName = configuration.Value.UserName,
                Password = configuration.Value.Password,
                VirtualHost = configuration.Value.VirtualHost
            };
            _queueName = configuration.Value.QueueName;
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.QueueDeclare(queue: configuration.Value.QueueName, durable: false, exclusive: false, autoDelete: false, arguments: null);
        }
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += (ch, ea) =>
            {
                var content = Encoding.UTF8.GetString(ea.Body.ToArray());
                try
                {
                    var requestPref = JsonConvert.DeserializeObject<GivePromoCodeRequest>(content);

                    using (var scope = _provider.CreateScope())
                    {
                        var preferences = _provider.GetService<IRepository<Preference>>();
                        var _customersRepository = _provider.GetService<IRepository<Customer>>();
                        var promocodes = _provider.GetService<IRepository<PromoCode>>();

                        var preference = preferences.GetByIdAsync(requestPref.PreferenceId).Result;

                        var customers = _customersRepository
                                        .GetWhere(d => d.Preferences.Any(x =>
                                            x.Preference.Id == preference.Id)).Result;

                        PromoCode promoCode = PromoCodeMapper.MapFromModel(requestPref, preference, customers);

                        promocodes.AddAsync(promoCode);

                    }

                    _channel.BasicAck(ea.DeliveryTag, false);

                }
                
                catch (System.Exception)
                {
                    _channel.BasicAck(ea.DeliveryTag, true);
                    //здесь можно доги добавить
                }

            };

            _channel.BasicConsume(_queueName, false, consumer);

            return Task.CompletedTask;
        }
    }
}