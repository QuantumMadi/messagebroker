﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain
{
    public class PromoCode
        : BaseEntity
    {
        public string Code { get; set; }

        public string ServiceInfo { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public Guid PartnerId { get; set; }
        
        [field: NonSerialized]       
        public virtual Partner Partner { get; set; }
        
        [field: NonSerialized]           
        public Guid? PartnerManagerId { get; set; }
        
        public virtual Preference Preference { get; set; }

        public Guid PreferenceId { get; set; }
    }
}