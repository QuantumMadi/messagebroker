using System;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Microsoft.Extensions.Options;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.ConfigurationModels;
using RabbitMQ.Client;
using Newtonsoft.Json;
using System.Text;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.MessageQueue
{
    public class QueueAdministrationGateway : IAdministrationGateway
    {
        private RabbitMqConfiguration _rabbitMq;
        private IConnection _connection;

        public QueueAdministrationGateway(IOptions<RabbitMqConfigurationOptions> configurations)
        {
            _rabbitMq = new RabbitMqConfiguration(configurations);
            _connection = _rabbitMq.GetConnection();
        }

        public Task NotifyAdminAboutPartnerManagerPromoCode(Guid partnerManagerId)
        {
            return Task.Run(() =>
            {
                if (_connection.IsOpen)
                {
                    using (var channel = _connection.CreateModel())
                    {
                        channel.QueueDeclare(_rabbitMq.GetQueueName(), false, false, false, null);
                        var json = JsonConvert.SerializeObject(partnerManagerId);
                        var bytes = Encoding.UTF8.GetBytes(json);
                        channel.BasicPublish(exchange: "", routingKey: _rabbitMq.GetQueueName(), basicProperties: null, body: bytes);
                    }                    
                    _connection.Dispose();
                }
            });

        }

    }
}