using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.ConfigurationModels;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto;
using RabbitMQ.Client;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.MessageQueue
{
    public class QueueGivingPromoCodeToCustomerGateway : IGivingPromoCodeToCustomerGateway
    { 
        private RabbitMqConfiguration _rabbitMq;
        private IConnection _connection;

        public QueueGivingPromoCodeToCustomerGateway(IOptions<RabbitMqConfigurationOptions> configurations)
        {
            _rabbitMq = new RabbitMqConfiguration(configurations);
            _connection = _rabbitMq.GetConnection();
        }
        public Task GivePromoCodeToCustomer(PromoCode promoCode)
        {
            return Task.Run(() =>
            {
                if (_connection.IsOpen)
                {
                    var dto = new GivePromoCodeToCustomerDto()
                    {
                        PartnerId = promoCode.Partner.Id,
                        BeginDate = promoCode.BeginDate.ToShortDateString(),
                        EndDate = promoCode.EndDate.ToShortDateString(),
                        PreferenceId = promoCode.PreferenceId,
                        PromoCode = promoCode.Code,
                        ServiceInfo = promoCode.ServiceInfo,
                        PartnerManagerId = promoCode.PartnerManagerId
                    };
                    using (var channel = _connection.CreateModel())
                    {
                        channel.QueueDeclare(_rabbitMq.GetQueueName(), false, false, false, null);
                        var json = JsonConvert.SerializeObject(dto);
                        var bytes = Encoding.UTF8.GetBytes(json);
                        channel.BasicPublish(exchange: "", routingKey: _rabbitMq.GetQueueName(), basicProperties: null, body: bytes);
                    }
                    _connection.Dispose();
                }
            });


        }
    }
}